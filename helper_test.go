package error

import (
	"gopkg.in/go-playground/assert.v1"
	"testing"
)

func TestToApplicationError(t *testing.T) {
	err := New("01", "error")
	_, ok := ToApplicationError(err)
	assert.Equal(t, ok, true)
}

func TestIsEqualError(t *testing.T) {
	err := New("01", "error")
	err2 := New("01", "error")
	equal := IsEqualError(err, err2)
	assert.Equal(t, equal, true)
}
