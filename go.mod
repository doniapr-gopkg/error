module gitlab.com/doniapr-gopkg/error

go 1.18

require (
	google.golang.org/grpc v1.56.1
	gopkg.in/go-playground/assert.v1 v1.2.1
)

require (
	github.com/golang/protobuf v1.5.3 // indirect
	google.golang.org/genproto v0.0.0-20230410155749-daa745c078e1 // indirect
	google.golang.org/protobuf v1.30.0 // indirect
)
