package error

import (
	"os"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// handling timeout from http and grpc
func IsTimeout(err error) (timeout bool) {
	timeout = os.IsTimeout(err)
	if timeout {
		return
	}

	st, ok := status.FromError(err)
	if !ok {
		return
	}

	if st.Code() == codes.DeadlineExceeded {
		timeout = true
	}

	return
}

func ToApplicationError(e error) (appError *ApplicationError, ok bool) {
	if e == nil {
		return
	}
	appError, ok = e.(*ApplicationError)
	return
}

func IsEqualError(e, er error) (equal bool) {
	err1, ok := ToApplicationError(e)
	if !ok {
		return
	}

	err2, ok := ToApplicationError(er)
	if !ok {
		return
	}

	if err1.ErrorCode == err2.ErrorCode {
		equal = true
		return
	}

	return
}
